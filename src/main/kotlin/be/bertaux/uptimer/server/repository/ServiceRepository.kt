package be.bertaux.uptimer.server.repository

import be.bertaux.uptimer.server.model.Service
import io.quarkus.hibernate.orm.panache.PanacheRepository
import javax.enterprise.context.ApplicationScoped

/**
 * Created by jeromebertaux on 30/12/2019.
 */
@ApplicationScoped
open class ServiceRepository : PanacheRepository<Service> {

}