package be.bertaux.uptimer.server.repository

import be.bertaux.uptimer.server.model.Status
import io.quarkus.hibernate.orm.panache.PanacheRepository
import io.quarkus.hibernate.orm.panache.runtime.JpaOperations
import javax.enterprise.context.ApplicationScoped

/**
 * Created by jeromebertaux on 30/12/2019.
 */
@ApplicationScoped
class StatusRepository : PanacheRepository<Status> {

    fun avgDuration(serviceId: Long): Double? {
        return JpaOperations.bindParameters(
                JpaOperations.getEntityManager().createQuery(
                        "select AVG(s.duration) from service_status s where s.service.id = :service_id"),
                mapOf("service_id" to serviceId)
        ).singleResult as? Double
    }

}
