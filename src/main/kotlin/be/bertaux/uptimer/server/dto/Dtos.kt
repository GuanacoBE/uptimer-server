package be.bertaux.uptimer.server.dto

/**
 * Created by jeromebertaux on 05/01/2020.
 */
data class ServiceDTO(val id: Long, val name: String, val uptime: Double)
