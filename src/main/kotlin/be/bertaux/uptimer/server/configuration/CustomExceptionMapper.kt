package be.bertaux.uptimer.server.configuration

import be.bertaux.uptimer.server.exception.NotFoundException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

/**
 * Created by jeromebertaux on 05/01/2020.
 */
@Provider
class CustomExceptionMapper : ExceptionMapper<Throwable> {

    private var log: Logger = LoggerFactory.getLogger(CustomExceptionMapper::class.java)

    override fun toResponse(e: Throwable?): Response {
        log.error("Error in the app", e)
        return when (e) {
            is IllegalArgumentException -> Response.status(Response.Status.BAD_REQUEST).build()
            is NotFoundException -> Response.status(Response.Status.GONE).build()
            else -> Response.status(Response.Status.INTERNAL_SERVER_ERROR).build()
        }
    }
}