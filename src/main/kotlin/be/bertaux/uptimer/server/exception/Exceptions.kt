package be.bertaux.uptimer.server.exception

/**
 * Created by jeromebertaux on 05/01/2020.
 */

class NotFoundException : RuntimeException()