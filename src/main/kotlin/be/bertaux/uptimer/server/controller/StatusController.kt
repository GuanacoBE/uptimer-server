package be.bertaux.uptimer.server.controller

import be.bertaux.uptimer.server.model.Status
import be.bertaux.uptimer.server.service.StatusService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

/**
 * Created by jeromebertaux on 30/12/2019.
 */
@Path("/services")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class StatusController @Inject constructor(private val statusService: StatusService) {

    private var log: Logger = LoggerFactory.getLogger(StatusController::class.java)

    @POST
    @Path("/{id}/status")
    fun create(@PathParam("id") serviceId: Long, status: Status): Status {
        log.info("CREATE STATUS")
        return statusService.create(serviceId, status)
    }

}
