package be.bertaux.uptimer.server.controller

import be.bertaux.uptimer.server.dto.ServiceDTO
import be.bertaux.uptimer.server.model.Service
import be.bertaux.uptimer.server.service.ServiceService
import be.bertaux.uptimer.server.service.StatusService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

/**
 * Created by jeromebertaux on 30/12/2019.
 */
@Path("/services")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class ServiceController @Inject constructor(private val serviceService: ServiceService,
                                            private val statusService: StatusService) {

    private var log: Logger = LoggerFactory.getLogger(ServiceController::class.java)

    @POST
    fun create(service: Service): Service {
        log.info("Create service")
        return serviceService.create(service)
    }

    @GET
    fun listUptime(): List<ServiceDTO> {
        log.info("List service")
        return serviceService.listUptime()
    }

    @GET
    @Path("/{id}/uptime")
    fun uptime(@PathParam("id") serviceId: Long): Double {
        log.info("Uptime service {}", serviceId)
        return statusService.computeUptime(serviceId)
    }

    @GET
    @Path("/{id}/average/duration")
    fun average(@PathParam("id") serviceId: Long): Double {
        log.info("Average service {}", serviceId)
        return statusService.avgDuration(serviceId)
    }

}