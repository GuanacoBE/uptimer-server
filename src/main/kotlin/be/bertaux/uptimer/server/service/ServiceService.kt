package be.bertaux.uptimer.server.service

import be.bertaux.uptimer.server.dto.ServiceDTO
import be.bertaux.uptimer.server.model.Service
import be.bertaux.uptimer.server.repository.ServiceRepository
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional

/**
 * Created by jeromebertaux on 30/12/2019.
 */
@ApplicationScoped
class ServiceService @Inject constructor(private val serviceRepository: ServiceRepository,
                                         private val statusService: StatusService) {

    fun listUptime(): List<ServiceDTO> {
        return serviceRepository.listAll()
                .map { s -> ServiceDTO(s.id, s.name!!, statusService.computeUptime(s.id)) }
    }

    @Transactional
    fun create(service: Service): Service {
        service.persist()
        return service
    }

}