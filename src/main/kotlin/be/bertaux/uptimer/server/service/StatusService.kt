package be.bertaux.uptimer.server.service

import be.bertaux.uptimer.server.exception.NotFoundException
import be.bertaux.uptimer.server.model.Status
import be.bertaux.uptimer.server.repository.ServiceRepository
import be.bertaux.uptimer.server.repository.StatusRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.transaction.Transactional

/**
 * Created by jeromebertaux on 30/12/2019.
 */
@ApplicationScoped
class StatusService @Inject constructor(private val serviceRepository: ServiceRepository,
                                        private val statusRepository: StatusRepository) {

    private var log: Logger = LoggerFactory.getLogger(StatusService::class.java)

    @Transactional
    fun create(serviceId: Long, status: Status): Status {
        status.service = serviceRepository.findById(serviceId)
        status.persist()
        return status
    }

    fun computeUptime(serviceId: Long): Double {
        serviceId.throwIf(serviceRepository.count("id", serviceId) != 1L)
        val total: Long = statusRepository.count("service.id", serviceId).takeIf { it != 0L }
                ?: throw NotFoundException()
        val totalUp: Long = statusRepository.count("service.id = ?1 AND code = '200'", serviceId)
        return totalUp.toDouble() / total.toDouble()
    }

    fun avgDuration(serviceId: Long): Double {
        serviceId.throwIf(serviceRepository.count("id", serviceId) != 1L)
        return statusRepository.avgDuration(serviceId) ?: throw NotFoundException()
    }

}

infix fun Long.throwIf(b: Boolean) {
    if (b) throw NotFoundException()
}