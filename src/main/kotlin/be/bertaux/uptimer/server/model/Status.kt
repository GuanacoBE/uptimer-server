package be.bertaux.uptimer.server.model

import io.quarkus.hibernate.orm.panache.PanacheEntity
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.ManyToOne

/**
 * Created by jeromebertaux on 30/12/2019.
 */
@Entity(name = "service_status")
class Status: PanacheEntity() {

    var code: String? = null

    var time: LocalDateTime? = null

    var duration: Int? = null

    @ManyToOne(optional = false)
    var service: Service? = null

}
