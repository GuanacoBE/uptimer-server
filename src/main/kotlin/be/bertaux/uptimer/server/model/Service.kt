package be.bertaux.uptimer.server.model

import io.quarkus.hibernate.orm.panache.PanacheEntity
import javax.json.bind.annotation.JsonbTransient
import javax.persistence.Entity
import javax.persistence.OneToMany

/**
 * Created by jeromebertaux on 30/12/2019.
 */

@Entity(name = "service")
class Service: PanacheEntity() {

    var name: String? = null

    var url: String? = null

    @OneToMany(orphanRemoval = true, mappedBy = "service")
    @JsonbTransient
    var statuses: List<Status> = emptyList()
}